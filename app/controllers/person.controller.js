const db = require("../models");
const Person = db.persons;
const Attendance = db.attendances;
const Op = db.Sequelize.Op;

// Create and Save a new Person
exports.createPerson = (person) => {
    return Person.create({
      name: person.name,
      firstname: person.firstname,
      type: person.type,
    })
      .then((person) => {
        console.log(">> Created person: " + JSON.stringify(person, null, 4));
        return person;
      })
      .catch((err) => {
        console.log(">> Error while creating person: ", err);
      });
  };

// Retrieve all Persons from the database.
exports.createAttendance = (personId, attendance) => {
    return Attendance.create({
      presence: attendance.presence,
      date: attendance.date,
      personId: personId,
    })
      .then((attendance) => {
        console.log(">> Created attendance: " + JSON.stringify(attendance, null, 4));
        return attendance;
      })
      .catch((err) => {
        console.log(">> Error while creating attendance: ", err);
      });
  };

// Find a single Person with an id
exports.findPersonById = (personId) => {
    return Person.findByPk(personId, { include: ["attendaces"] })
      .then((person) => {
        return person;
      })
      .catch((err) => {
        console.log(">> Error while finding person: ", err);
      });
  };


exports.findAttendanceById = (id) => {
    return Attendance.findByPk(id, { include: ["person"] })
      .then((attendance) => {
        return attendance;
      })
      .catch((err) => {
        console.log(">> Error while finding attendance: ", err);
      });
  };
  
  // exports.findAll = () => {
  //   return Person.findAll({
  //     include: ["attendances"],
  //   }).then((persons) => {
  //     return persons;
  //   });
  // };

  // Create and Save a new Person
exports.create = (req, res) => {
    // Validate request
    if (!req.body.name) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
  
    // Create a Person
    const person = {
      name: req.body.name,
      firstname: req.body.firstname,
      type: req.body.type 
    };
  
    // Save Person in the database
    Person.create(person)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Person."
        });
      });
  };
  
  // Retrieve all Persons from the database.
  exports.findAll = (req, res) => {
    const name = req.query.name;
    var condition = name ? { name: { [Op.iLike]: `%${name}%` } } : null;
  
    Person.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving persons."
        });
      });
  };
  
  // Find a single Person with an id
  exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Person.findByPk(id)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving Person with id=" + id
        });
      });
  };
  
  // Update a Person by the id in the request
  exports.update = (req, res) => {
    const id = req.params.id;
  
    Person.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Person was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Person with id=${id}. Maybe Person was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Person with id=" + id
        });
      });
  };
  
  // Delete a Person with the specified id in the request
  exports.delete = (req, res) => {
    const id = req.params.id;
  
    Person.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Person was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Person with id=${id}. Maybe Person was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Person with id=" + id
        });
      });
  };
  
  // Delete all Persons from the database.
  exports.deleteAll = (req, res) => {
    Person.destroy({
      where: {},
      truncate: false
    })
      .then(nums => {
        res.send({ message: `${nums} Persons were deleted successfully!` });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all persons."
        });
      });
  };
  
  // find all present Person
  exports.findAllPresent = (req, res) => {
    Attendance.findAll({ where: { presence: true } })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving attendances."
        });
      });
  };
  