module.exports = (sequelize, Sequelize) => {
    const Attendance = sequelize.define("attendance", {
      presence: {
        type: Sequelize.BOOLEAN
      },
      date: {
        type: Sequelize.DATE
      }
    });
  
    return Attendance;
  };