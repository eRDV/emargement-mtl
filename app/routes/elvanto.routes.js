module.exports = (app) => {
    var elvanto = require("elvanto-api");

    var router = require("express").Router();

    router.get("/getPeople", (req, res) => {
        let name = req.query.name;

        try {
            elvanto.configure({ accessToken: app.get("token") });
            getPeople(elvanto, res, name);
        } catch (e) {
            console.log(e.message);
            if (e.message.includes("token")) {
                // refresh token
                elvanto.refreshToken(app.get("refreshToken"), (data) => {
                    console.log("refresh data :", data);
                    setToken(app, data);
                    elvanto.configure({ accessToken: app.get("token") });
                    getPeople(elvanto, res, name);
                });
            }
            res.send(e.message);
        }
    });
    process.on("unhandledRejection", (error, promise) => {
        console.log(
            " Oh Lord! We forgot to handle a promise rejection here: ",
            promise
        );
        console.log(" The error was: ", error);
    });
    app.use("/elvanto/", router);
};
function setToken(app, data) {
    app.set("token", data.access_token);
    app.set("refreshToken", data.refresh_token);
}

function getPeople(elvanto, res, name) {
    try {
        elvanto.apiCall(
            "people/search",
            {
                search: { lastname: name },
                fields: [
                    "custom_dd39f680-7a12-476c-af96-ed0de1c97031",
                    "custom_d941312d-97ac-412f-aab9-c3db0bc20b67",
                    "custom_963b31ec-c660-4828-a1b7-5cc9688518dd",
                    "custom_beae2dc4-d225-41ce-84ec-be713f7ef623",
                    "custom_74c5f676-ad54-4819-b78b-e1d0ee88e53b",
                    "custom_f70b5191-930b-4390-9a3b-f1ad72a7f480",
                ], //validation 001, 101, 201 et presences 001,101,201
            },
            (data) => {
                console.log("result data :", data);
                res.send(JSON.stringify(data.people));
            }
        );
    } catch (e) {
        res.send(e.message);
    }
}
