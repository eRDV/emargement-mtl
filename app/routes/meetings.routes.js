module.exports = (app) => {
    const jwt = require("jsonwebtoken");
    const config = require("../config/config");
    const axios = require("axios");

    //Use the ApiKey and APISecret from config.js
    const payload = {
        iss: config.APIKey,
        exp: new Date().getTime() + 5000,
    };
    const token = jwt.sign(payload, config.APISecret);

    var router = require("express").Router();

    router.get("/:id", (req, res) => {
        //store the email address of the user in the email variable
        let id = req.params.id;

        //check if the email was stored in the console
        console.log(id);
        let url="https://api.zoom.us/v2/past_meetings/" + id + "/instances";
        var options = {
            params: {
                status: "active",
            },
            headers: {
                "User-Agent": "Zoom-api-Jwt-Request",
                "content-type": "application/json",
                Authorization: "Bearer " + token,
            },
        };
        axios
        .get(url, options)
        .then(function (response) {
            console.log("response: ", response.data);
            //Prettify the JSON format using pre tag and JSON.stringify
            var result = JSON.stringify(response.data);
            console.log(response.data.meetings[0].uuid);
            res.send(result);
        })
        .catch(function (err) {
            // API call failed...
            console.log("API call failed, reason ", err);
        });
        
    });

    // Retrieve participants
    router.get("/participants/:uuid", (req, res) => {
        //store the email address of the user in the email variable
        let uuid = req.params.uuid;
        let next_token = req.query.next_page_token;
        //check if the email was stored in the console
        console.log(uuid);
        console.log(typeof next_token);
        let participantsJSON = null;

        getParticipants(next_token, participantsJSON).then((data) => {
            var result = JSON.stringify(data);
            //console.log("results", result);
            res.send(result);
        });

        function getParticipants(next_page, data) {
            let url = "https://api.zoom.us/v2/metrics/meetings/" + uuid + "/participants";
            if (
                next_page !== "undefined" &&
                next_page !== undefined &&
                next_page !== null
            ) {
                url = url + "?next_page_token=" + next_page;
            }
            var options = {
                params: {
                    type: "past",
                    page_size: 100,
                    status: "active",
                },
                headers: {
                    "User-Agent": "Zoom-api-Jwt-Request",
                    "content-type": "application/json",
                    Authorization: "Bearer " + token,
                },
            };
            return axios
                .get(url, options)
                .then(function (response) {
                    if (response.data.length < 1) return data;
                    if (data === null) {
                        data = response.data;
                    } else {
                        console.log("part before: ", data.participants.length);
                        data.participants.push(...response.data.participants);
                        console.log("part after: ", data.participants.length);
                        console.log('response.data.next_page_token',response.data.next_page_token);
                        if(response.data.next_page_token===null || response.data.next_page_token===undefined || response.data.next_page_token==='undefined' || response.data.next_page_token.trim()===''){
                          console.log('response.data.next_page_token',response.data.next_page_token);
                          console.log('data: ',data);
                          return data;
                        }
                    }

                    return getParticipants(response.data.next_page_token, data);
                })
                .catch(function (err) {
                    // API call failed...
                    console.log("API call failed, reason ", err);
                });
        }
    });

    // Retrieve a single Person with id
    router.get("/account/:email", (req, res) => {
        //store the email address of the user in the email variable
        let email = req.params.email;

        //check if the email was stored in the console
        console.log(email);

        //Store the options for Zoom API which will be used to make an API call later.
        let url = "https://api.zoom.us/v2/users/" + email + "/meetings";
        //options
        var options = {
            params: {
                type: "scheduled",
                page_size: 100,
            },
            headers: {
                "User-Agent": "Zoom-api-Jwt-Request",
                "content-type": "application/json",
                Authorization: "Bearer " + token,
            },
        };

        axios
            .get(url, options)
            .then(function (response) {
                console.log("response: ", response.data);
                //Prettify the JSON format using pre tag and JSON.stringify
                var result = JSON.stringify(response.data);
                console.log(response.data.meetings[0].topic);
                res.send(result);
            })
            .catch(function (err) {
                // API call failed...
                console.log("API call failed, reason ", err);
            });
    });

    app.use("/api/meetings", router);
};
