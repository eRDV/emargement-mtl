const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const https = require('https');
const fs = require('fs');

var key = fs.readFileSync('/etc/letsencrypt/live/erdv.ch/privkey.pem');
var cert = fs.readFileSync('/etc/letsencrypt/live/erdv.ch/fullchain.pem');

var credentials = {
  key: key,
  cert: cert
};



const app = express();

var corsOptions = {
  origin: "https://erdv.ch:3000"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());


// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");
db.sequelize.sync();
// db.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync db.");
//   run();
// });

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to eRDV server." });
});

require("./app/routes/person.routes")(app);
require("./app/routes/meetings.routes")(app);
require("./app/routes/elvanto.login.routes")(app);
require("./app/routes/elvanto.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 12015;

var httpsServer = https.createServer(credentials, app);

httpsServer.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
