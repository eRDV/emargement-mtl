import React from "react";
import { HashRouter as Router, Route, Routes, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import MeetingsList from "./components/meetings-list.component";

class App extends React.Component {
  render() {
    const linkStyle = {
      color: "#82A7DC",
      textDecoration: "none",
    };

    return (
      <Router>
        <React.Fragment>
          {/* Page header */}
          <nav className="navbar navbar-expand navbar-dark bg-dark">
            <a
              href="https://formations.egliseicc.com"
              target="_blank"
              className="navbar-brand"
            >
              PCNC
            </a>
            <div className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to={"/meetings"} className="nav-link">
                  Mettings Zoom
                </Link>
              </li>
            </div>
          </nav>

          <div className="container mt-3">
            <Routes>
              <Route path="/meetings" element={<MeetingsList />} />
            </Routes>
          </div>
        </React.Fragment>
      </Router>
    );
  }
}

export default App;
