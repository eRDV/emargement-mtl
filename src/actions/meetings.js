import MeetingDataService from "../services/meeting.service";
  
export const retrieveMeetings = async (id) =>  {
  try {
    const res = await MeetingDataService.getAll(id);
    return res.data;
    
  } catch (err) {
    console.log(err);
  }
};

export const retrieveParticipants = async (uuid, next_page_token) =>  {
  try {
    const res = await MeetingDataService.getParticipants(uuid,next_page_token);
    return res.data;
    
  } catch (err) {
    console.log(err);
  }
};

export const retrieveAccountMeetings = async (email) =>  {
  try {
    const res = await MeetingDataService.getAccountMettings(email);
    return res.data;
    
  } catch (err) {
    console.log(err);
  }
};
  
  
