import {
    CREATE_PERSON,
    RETRIEVE_PERSONS,
    UPDATE_PERSON,
    DELETE_PERSON,
    DELETE_ALL_PERSONS
  } from "./types";
  
  import PersonDataService from "../services/person.service";
  
  export const createPerson = (name, firstname, type) => async (dispatch) => {
    try {
      const res = await PersonDataService.create({ name, firstname, type });
  
      dispatch({
        type: CREATE_PERSON,
        payload: res.data,
      });
  
      return Promise.resolve(res.data);
    } catch (err) {
      return Promise.reject(err);
    }
  };
  
  export const retrievePersons = () => async (dispatch) => {
    try {
      const res = await PersonDataService.getAll();
  
      dispatch({
        type: RETRIEVE_PERSONS,
        payload: res.data,
      });
    } catch (err) {
      console.log(err);
    }
  };
  
  export const updatePerson = (id, data) => async (dispatch) => {
    try {
      const res = await PersonDataService.update(id, data);
  
      dispatch({
        type: UPDATE_PERSON,
        payload: data,
      });
  
      return Promise.resolve(res.data);
    } catch (err) {
      return Promise.reject(err);
    }
  };
  
  export const deletePerson = (id) => async (dispatch) => {
    try {
      await PersonDataService.delete(id);
  
      dispatch({
        type: DELETE_PERSON,
        payload: { id },
      });
    } catch (err) {
      console.log(err);
    }
  };
  
  export const deleteAllPersons = () => async (dispatch) => {
    try {
      const res = await PersonDataService.deleteAll();
  
      dispatch({
        type: DELETE_ALL_PERSONS,
        payload: res.data,
      });
  
      return Promise.resolve(res.data);
    } catch (err) {
      return Promise.reject(err);
    }
  };
  
  export const findPersonsByName = (name) => async (dispatch) => {
    try {
      const res = await PersonDataService.findByName(name);
  
      dispatch({
        type: RETRIEVE_PERSONS,
        payload: res.data,
      });
    } catch (err) {
      console.log(err);
    }
  };