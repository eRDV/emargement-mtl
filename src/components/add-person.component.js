import React, { Component } from "react";
import { connect } from "react-redux";
import { createPerson } from "../actions/persons";

class AddPerson extends Component {
  constructor(props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeFirstname = this.onChangeFirstname.bind(this);
    this.onChangeType = this.onChangeType.bind(this);
    this.savePerson = this.savePerson.bind(this);
    this.newPerson = this.newPerson.bind(this);

    this.state = {
      id: null,
      name: "",
      firstname: "",
      type: false,

      submitted: false,
    };
  }

  onChangeName(e) {
    this.setState({
      name: e.target.value,
    });
  }

  onChangeFirstname(e) {
    this.setState({
      firstname: e.target.value,
    });
  }

  onChangeType(e) {
    this.setState({
      type: e.target.value,
    });
  }

  savePerson() {
    const { name, firstname, type } = this.state;

    this.props
      .createPerson(name, firstname, type)
      .then((data) => {
        this.setState({
          id: data.id,
          name: data.name,
          firstname: data.firstname,
          type: data.type,

          submitted: true,
        });
        console.log(data);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  newPerson() {
    this.setState({
      id: null,
      name: "",
      firstname: "",
      type: "",

      submitted: false,
    });
  }

  render() {
    return (
        <div className="submit-form">
          {this.state.submitted ? (
            <div>
              <h4>You submitted successfully!</h4>
              <button className="btn btn-success" onClick={this.newPerson}>
                Add
              </button>
            </div>
          ) : (
            <div>
              <div className="form-group">
                <label htmlFor="title">Nom</label>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  required
                  value={this.state.name}
                  onChange={this.onChangeName}
                  name="name"
                />
              </div>
  
              <div className="form-group">
                <label htmlFor="description">Prenom</label>
                <input
                  type="text"
                  className="form-control"
                  id="firstname"
                  required
                  value={this.state.firstname}
                  onChange={this.onChangeFirstname}
                  name="firstname"
                />
              </div>

              <div className="form-group">
                <label htmlFor="description">Type</label>
                <input
                  type="text"
                  className="form-control"
                  id="type"
                  required
                  value={this.state.type}
                  onChange={this.onChangeType}
                  name="type"
                />
              </div>
  
              <button onClick={this.savePerson} className="btn btn-success">
                Submit
              </button>
            </div>
          )}
        </div>
      );
  }
}

export default connect(null, { createPerson })(AddPerson);