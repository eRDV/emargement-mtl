import React, { Component } from "react";
import { connect } from "react-redux";
import MeetingDataService from "../services/meeting.service";

class Meeting extends Component {
  constructor(props) {
    super(props);
    this.getMeeting = this.getMeeting.bind(this);
    
    this.state = {
      currentMeeting: {
        id: null,
        uuid: "",
        start_time: "",
      },
      message: "",
    };
  }

  componentDidMount() {
    this.getMeeting(this.props.match.params.id);
  }

  onChangeId(e) {
    const uuid = e.target.value;

    this.setState(function (prevState) {
      return {
        currentMeeting: {
          ...prevState.currentMeeting,
          uuid: uuid,
        },
      };
    });
  }

  getMeeting(id) {
    MeetingDataService.get(id)
      .then((response) => {
        this.setState({
          currentMeeting: response.data,
        });
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }



  render() {
    const { currentMeeting } = this.state;

    return (
        <div>
        {currentMeeting ? (
          <div classId="edit-form">
            <h4>Meeting</h4>
            <form>
              <div classId="form-group">
                <label htmlFor="name">UUID</label>
                <input
                  type="text"
                  classId="form-control"
                  id="uuid"
                  value={currentMeeting.uuid}
                  
                />
              </div>
              <div classId="form-group">
                <label htmlFor="description">Date</label>
                <input
                  type="text"
                  classId="form-control"
                  id="date"
                  value={currentMeeting.start_time}
                  
                />
              </div>
              

              {/* <div classId="form-group">
                <label>
                  <strong>Status:</strong>
                </label>
                {currentMeeting.published ? "Published" : "Pending"}
              </div> */}
            </form>

            {/* {currentMeeting.published ? (
              <button
                classId="badge badge-primary mr-2"
                onClick={() => this.updateStatus(false)}
              >
                UnPublish
              </button>
            ) : (
              <button
                classId="badge badge-primary mr-2"
                onClick={() => this.updateStatus(true)}
              >
                Publish
              </button>
            )} */}



            
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <br />
            <p>Selectionnez une meeting ... </p>
          </div>
        )}
      </div>
    );
  }
}

export default connect(null)(Meeting);