import { parseISO } from "date-fns";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  retrieveAccountMeetings,
  retrieveMeetings,
  retrieveParticipants,
} from "../actions/meetings";
import { RETRIEVE_MEETINGS, RETRIEVE_PARTICIPANTS } from "../actions/types";
import { format } from 'date-fns';
import { addMilliseconds } from 'date-fns';
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from '@mui/material/Button';
import { DataGrid } from '@mui/x-data-grid';


class MeetingsList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchMeetingId = this.onChangeSearchMeetingId.bind(this);
    this.onChangeSearchMeetingEmail =
      this.onChangeSearchMeetingEmail.bind(this);
    this.refreshData = this.refreshData.bind(this);
    this.setActiveMeeting = this.setActiveMeeting.bind(this);
    this.findById = this.findById.bind(this);
    this.getParticipants = this.getParticipants.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);

    this.state = {
      currentMeeting: null,
      currentIndex: -1,
      searchMeetingId: "",
      searchMeetingEmail: "",
      currentPartIndex: -1,
      currentParticipant: null,
      currentPage: 1,
      currentUuid: null,
      selectedAccount: null,
    };
  }

  componentDidMount() { }

  onChangeSearchMeetingId(e) {
    const searchMeetingId = e.target.value;

    this.setState({
      searchMeetingId: searchMeetingId,
    });
  }

  onChangeSearchMeetingEmail(e) {
    const searchMeetingEmail = e.target.value;

    this.setState({
      searchMeetingEmail: searchMeetingEmail,
    });
  }

  refreshData() {
    this.setState({
      currentMeeting: null,
      currentIndex: -1,
    });
  }

  refreshParticipants() {
    this.setState({
      currentParticipant: null,
      currentPartIndex: -1,
      currentUuid: null,
    });
  }

  setActiveMeeting(meeting, index) {
    this.setState({
      currentMeeting: meeting,
      currentIndex: index,
    });
  }

  setActiveParticipant(participant, index) {
    this.setState({
      currentParticipant: participant,
      currentPartIndex: index,
    });
  }

  async findById() {
    this.refreshData();
    let meetings = null;
    const accountMeetings = await retrieveAccountMeetings(
      this.state.searchMeetingEmail.trim()
    );
    this.state.selectedAccount = accountMeetings.meetings.find(
      (meetings) => meetings.id === parseInt(this.state.searchMeetingId)
    );
    console.log("--SelectedMeeting Account--");
    console.log(this.state.selectedAccount);
    if (this.state.selectedAccount !== undefined) {
      console.log("retrive meetings");
      meetings = await retrieveMeetings(this.state.searchMeetingId.trim());
      const sortDsc = this.sortDsc();
      sortDsc(meetings.meetings, "start_time");
    } else {
      let participants = null;
      this.props.dispatch({
        type: RETRIEVE_PARTICIPANTS,
        payload: participants,
      });
    }
    this.props.dispatch({
      type: RETRIEVE_MEETINGS,
      payload: meetings,
    });
  }

  async getParticipants(uuid, next_page_token) {
    this.refreshParticipants();
    let participants = await retrieveParticipants(uuid.trim(), next_page_token);
    this.state.currentUuid = uuid;
    var temp = [];
    if (participants === undefined) {
      participants = [];
    } else {
      participants.participants = participants.participants.filter((item) => {
        if (!temp.includes(item.ip_address)) {
          temp.push(item.ip_address);
          return true;
        }
      });
      // sort
      participants.participants = participants.participants.filter((item) => {
        if (!temp.includes(item.user_name)) {
          temp.push(item.user_name);
          return true;
        }
      });

      const sortAsc = this.sortAsc();
      sortAsc(participants.participants, "user_name");
    }

    this.props.dispatch({
      type: RETRIEVE_PARTICIPANTS,
      payload: participants,
    });
  }

  sortAsc() {
    return (arr, field) => {
      return arr.sort((a, b) => {
        if (a[field] > b[field]) {
          return 1;
        }
        if (b[field] > a[field]) {
          return -1;
        }
        return 0;
      });
    };
  }

  sortDsc() {
    return (arr, field) => {
      return arr.sort((a, b) => {
        if (a[field] < b[field]) {
          return 1;
        }
        if (b[field] < a[field]) {
          return -1;
        }
        return 0;
      });
    };
  }

  handlePageChange(page, next_token) {
    this.setState({ currentPage: page });
    this.getParticipants(this.state.currentUuid, next_token);
    this.state.currentPage += 1;
  }

  render() {
    const {
      searchMeetingId,
      searchMeetingEmail,
      currentIndex,
      currentPartIndex,
      selectedAccount,
    } = this.state;
    const { meetings, participants } = this.props;
    console.log(typeof this.props);
    console.log("----- meets -----");
    console.log(meetings);
    console.log("----- participants -----");
    console.log(participants);
    console.log(participants && participants.page_size === undefined);

    return (
      <div className="list row">
        <div className="col-md-12">
          <div className="input-group mb-3">
            <Box
              component="form"
              sx={{
                "& > :not(style)": { m: 1, width: "40ch" },
              }}
              noValidate
              autoComplete="off"
            >
              <TextField
                id="email_zoom"
                label="Email du compte zoom"
                variant="outlined"
                value={searchMeetingEmail}
                onChange={this.onChangeSearchMeetingEmail}
              />
              <TextField
                id="id_meeting"
                label="Id de meeting"
                variant="outlined"
                value={searchMeetingId}
                onChange={this.onChangeSearchMeetingId}
              />
              <Button
                variant="contained"
                onClick={this.findById}
              >
                Recherche
              </Button>
            </Box>
          </div>
        </div>
        <h2>
          Meetings de la classe{" "}
          {this.state.selectedAccount !== null &&
            this.state.selectedAccount !== undefined &&
            this.state.selectedAccount !== "undefined"
            ? this.state.selectedAccount.topic
            : ""}
        </h2>
        <br />
        <div className="col-md-6" style={{ height: 500 }}>
          <DataGrid
            columns={[{ field: 'start_time' }]}
            columns={[
              {
                type: 'date',
                field: 'start_time',
                headerName: 'Liste des sessions',
                flex: 1,
                minWidth: 150,
                valueFormatter: (param) => {
                  //console.log(param);
                  const valueFormatted = parseISO(param.value).toLocaleDateString("fr-FR", {
                    weekday: "long",
                    year: "numeric",
                    month: "long",
                    day: "numeric",
                  })
                  console.log(`${valueFormatted}`);
                  return `${valueFormatted}`;

                },
                renderCell: (cellValues) => {
                  return (
                    <div
                      style={{
                        color: "blue",
                        fontSize: 18,
                        width: "100%",
                        textAlign: "left"
                      }}
                    >
                      {parseISO(cellValues.value).toLocaleDateString("fr-FR", {
                        weekday: "long",
                        year: "numeric",
                        month: "long",
                        day: "numeric",
                      })}
                    </div>
                  );
                }
              },
            ]}
            autoPageSize = {true}
            rows={meetings &&
              meetings.meetings !== undefined ? meetings.meetings : []}
            onRowClick={(param) => {
              console.log("onRowClick");
              console.log(param);
              this.setActiveMeeting(param.row, 1);
              this.getParticipants(param.row.uuid);
              this.state.currentPage = 1;
            }}
            getRowId={(param) => {
              return param.uuid;
            }}
          />

        </div>
        <div className="col-md-6" style={{ height: 500 }}>
          <DataGrid
            columns={[
              {
                type: "text",
                field: "user_name",
                headerName: "Participant",
                flex: 1,
                minWidth: 150,
                renderCell: (cellValues) => {
                  return (
                    <div
                      style={{
                        color: "blue",
                        fontSize: 13,
                        width: "100%",
                        textAlign: "left"
                      }}
                    >
                      {cellValues.value}
                    </div>
                  );
                }
              },
              {
                type: "text",
                field: "email",
                headerName: "Email",
                flex: 1,
                minWidth: 150,
                renderCell: (cellValues) => {
                  return (
                    <div
                      style={{
                        color: "blue",
                        fontSize: 13,
                        width: "100%",
                        textAlign: "left"
                      }}
                    >
                      {cellValues.value}
                    </div>
                  );
                }
              },
              {
                type: "number",
                field: "duration",
                headerName: "Duree",
                flex: 1,
                minWidth: 150,

                valueGetter: (param) => {
                  //console.log(param);
                  const valueFormatted =
                    parseISO(param.row.leave_time).getTime() - parseISO(param.row.join_time).getTime();
                  //console.log(`${valueFormatted}`);
                  return `${valueFormatted}`;
                },
                renderCell: (cellValues) => {
                  return (
                    <div
                      style={{
                        color: "blue",
                        fontSize: 13,
                        width: "100%",
                        textAlign: "left"
                      }}
                    >
                      {format(addMilliseconds(new Date(0 + new Date(0).getTimezoneOffset() * 1000 * 60), parseInt(cellValues.value)), "HH' h 'mm' minutes'")}
                    </div>
                  );
                }
              }
            ]}
            //autoPageSize = {true}
            pageSize = {100}
           // pagination
           // paginationMode="server"
           // onPageChange={(page) =>
           //   this.handlePageChange(page, participants.next_page_token)}
            //rowCount={participants !== null ? participants.total_records : 0}
           // page={this.state.currentPage}
            rows={participants &&
              participants.participants !== undefined ? participants.participants : []}
            getRowId={(param) => {
              return param.user_id;
            }}
          />


          {/* <h4>
            Liste des participants <br /> (
            {participants === null || participants.total_records === undefined
              ? 0
              : participants.total_records}{" "}
            incluant des doublons)
          </h4>
          <ul className="list-group">
            {participants &&
              participants.participants &&
              participants.participants.map((participant, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentPartIndex ? "active" : "")
                  }
                  // onClick={() => {this.setActiveMeeting(meeting, index); this.retrieveParticipants();}}
                  key={index}
                >
                  {participant.user_name}
                </li>
              ))}
          </ul> */}
          {/* <Pagination
            pageSize={
              participants === null || participants.page_size === undefined
                ? 0
                : participants.page_size
            }
            onChange={(page) =>
              this.handlePageChange(page, participants.next_page_token)
            }
            current={this.state.currentPage}
            total={
              participants === null || participants.total_records === undefined
                ? 0
                : participants.total_records
            }
            hideOnSinglePage={true}
          /> */}
          {/* <Stack spacing={2}>
            <Pagination
              count={participants === null || participants.page_count === undefined
                ? 0
                : participants.page_count}
              page={this.state.currentPage}
              onChange={(page, value) =>
                this.handlePageChange(value, participants.next_page_token !== undefined ? participants.next_page_token : '0')
              }
              color="primary"
            />
          </Stack> */}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    meetings: state.meetings,
    participants: state.participants,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
  };
};

//export default MeetingsList;
export default connect(mapStateToProps, mapDispatchToProps)(MeetingsList);
