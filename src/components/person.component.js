import React, { Component } from "react";
import { connect } from "react-redux";
import { updatePerson, deletePerson } from "../actions/persons";
import PersonDataService from "../services/person.service";

class Person extends Component {
  constructor(props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeFirstname = this.onChangeFirstname.bind(this);
    this.onChangeType = this.onChangeType.bind(this);
    this.getPerson = this.getPerson.bind(this);
    this.updateStatus = this.updateStatus.bind(this);
    this.updateContent = this.updateContent.bind(this);
    this.removePerson = this.removePerson.bind(this);

    this.state = {
      currentPerson: {
        id: null,
        name: "",
        firstname: "",
        type: "",
      },
      message: "",
    };
  }

  componentDidMount() {
    this.getPerson(this.props.match.params.id);
  }

  onChangeName(e) {
    const name = e.target.value;

    this.setState(function (prevState) {
      return {
        currentPerson: {
          ...prevState.currentPerson,
          name: name,
        },
      };
    });
  }

  onChangeFirstname(e) {
    const firstname = e.target.value;

    this.setState((prevState) => ({
      currentPerson: {
        ...prevState.currentPerson,
        firstname: firstname,
      },
    }));
  }

  onChangeType(e) {
    const type = e.target.value;

    this.setState((prevState) => ({
      currentPerson: {
        ...prevState.currentPerson,
        type: type,
      },
    }));
  }

  getPerson(id) {
    PersonDataService.get(id)
      .then((response) => {
        this.setState({
          currentPerson: response.data,
        });
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  updateStatus(status) {
    var data = {
      id: this.state.currentPerson.id,
      name: this.state.currentPerson.title,
      firstname: this.state.currentPerson.description,
      type: this.state.currentPerson.type,
    };

    this.props
      .updatePerson(this.state.currentPerson.id, data)
      .then((reponse) => {
        console.log(reponse);

        this.setState((prevState) => ({
          currentPerson: {
            ...prevState.currentPerson,
            published: status,
          },
        }));

        this.setState({ message: "The status was updated successfully!" });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  updateContent() {
    this.props
      .updatePerson(this.state.currentPerson.id, this.state.currentPerson)
      .then((reponse) => {
        console.log(reponse);
        
        this.setState({ message: "The person was updated successfully!" });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  removePerson() {
    this.props
      .deletePerson(this.state.currentPerson.id)
      .then(() => {
        this.props.history.push("/persons");
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { currentPerson } = this.state;

    return (
        <div>
        {currentPerson ? (
          <div className="edit-form">
            <h4>Personne</h4>
            <form>
              <div className="form-group">
                <label htmlFor="name">Nom</label>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  value={currentPerson.name}
                  onChange={this.onChangeName}
                />
              </div>
              <div className="form-group">
                <label htmlFor="description">Prenom</label>
                <input
                  type="text"
                  className="form-control"
                  id="firstname"
                  value={currentPerson.firstname}
                  onChange={this.onChangeFirstname}
                />
              </div>
              <div className="form-group">
                <label htmlFor="description">Type</label>
                <input
                  type="text"
                  className="form-control"
                  id="type"
                  value={currentPerson.type}
                  onChange={this.onChangeType}
                />
              </div>

              {/* <div className="form-group">
                <label>
                  <strong>Status:</strong>
                </label>
                {currentPerson.published ? "Published" : "Pending"}
              </div> */}
            </form>

            {/* {currentPerson.published ? (
              <button
                className="badge badge-primary mr-2"
                onClick={() => this.updateStatus(false)}
              >
                UnPublish
              </button>
            ) : (
              <button
                className="badge badge-primary mr-2"
                onClick={() => this.updateStatus(true)}
              >
                Publish
              </button>
            )} */}

            <button
              className="badge badge-danger mr-2"
              onClick={this.removePerson}
            >
              Effacer
            </button>

            <button
              type="submit"
              className="badge badge-success"
              onClick={this.updateContent}
            >
              Mettre a jour
            </button>
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <br />
            <p>Selectionnez une personne ... </p>
          </div>
        )}
      </div>
    );
  }
}

export default connect(null, { updatePerson, deletePerson })(Person);