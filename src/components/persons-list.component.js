import React, { Component } from "react";
import { connect } from "react-redux";
import {
  retrievePersons,
  findPersonsByName,
  deleteAllPersons,
} from "../actions/persons";
import { Link } from "react-router-dom";


class PersonsList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchName = this.onChangeSearchName.bind(this);
    this.refreshData = this.refreshData.bind(this);
    this.setActivePerson = this.setActivePerson.bind(this);
    this.findByName = this.findByName.bind(this);
    this.removeAllPersons = this.removeAllPersons.bind(this);

    this.state = {
      currentPerson: null,
      currentIndex: -1,
      searchName: "",
    };
  }

  componentDidMount() {
    this.props.retrievePersons();
  }

  onChangeSearchName(e) {
    const searchName = e.target.value;

    this.setState({
      searchName: searchName,
    });
  }

  refreshData() {
    this.setState({
      currentPerson: null,
      currentIndex: -1,
    });
  }

  setActivePerson(person, index) {
    this.setState({
      currentPerson: person,
      currentIndex: index,
    });
  }

  removeAllPersons() {
    this.props
      .deleteAllPersons()
      .then((response) => {
        console.log(response);
        this.refreshData();
      })
      .catch((e) => {
        console.log(e);
      });
  }

  findByName() {
    this.refreshData();

    this.props.findPersonsByName(this.state.searchName);
  }

  render() {
    const { searchName, currentPerson, currentIndex } = this.state;
    const { persons } = this.props;

    return (
      <div className="list row">
        <div className="col-md-8">
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Recherche par nom"
              value={searchName}
              onChange={this.onChangeSearchName}
            />
            <div className="input-group-append">
              <button
                className="btn btn-outline-secondary"
                type="button"
                onClick={this.findByName}
              >
                Recherche
              </button>
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <h4>Liste des personnes</h4>

          <ul className="list-group">
            {persons &&
              persons.map((person, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActivePerson(person, index)}
                  key={index}
                >
                  {person.name}
                </li>
              ))}
          </ul>

          <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllPersons}
          >
            Tout effacer
          </button>
        </div>
        <div className="col-md-6">
          {currentPerson ? (
            <div>
              <h4>Personne</h4>
              <div>
                <label>
                  <strong>Nom:</strong>
                </label>{" "}
                {currentPerson.name}
              </div>
              <div>
                <label>
                  <strong>Prenom:</strong>
                </label>{" "}
                {currentPerson.firstname}
              </div>
              <div>
                <label>
                  <strong>Type:</strong>
                </label>{" "}
                {currentPerson.type}
              </div>
              {/* <div>
                <label>
                  <strong>Status:</strong>
                </label>{" "}
                {currentPerson.published ? "Published" : "Pending"}
              </div> */}

              <Link
                to={"/persons/" + currentPerson.id}
                className="badge badge-warning"
              >
                Modifier
              </Link>
            </div>
          ) : (
            <div>
              <br />
              <p>Selectionnez une personne</p>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    persons: state.persons,
  };
};

export default connect(mapStateToProps, {
  retrievePersons,
  findPersonsByName,
  deleteAllPersons,
})(PersonsList);
