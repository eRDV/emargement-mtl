import axios from "axios";

export default axios.create({
  baseURL: "https://erdv.ch:12015/api",
  headers: {
    "Content-type": "application/json"
  }
});
