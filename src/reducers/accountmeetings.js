import { RETRIEVE_ACCOUNT_MEETINGS } from "../actions/types";

const initialState = [];

function accountMeetingsReducer(accountmeetings = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case RETRIEVE_ACCOUNT_MEETINGS:
      return payload;

    default:
      return accountmeetings;
  }
}

export default accountMeetingsReducer;
