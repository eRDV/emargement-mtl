import { combineReducers } from "redux";
import persons from "./persons";
import meetings from "./meetings";
import participants from "./participants";
import accountmeetings from "./accountmeetings";

export default combineReducers({
  persons,
  meetings,
  participants,
  accountmeetings,
});
