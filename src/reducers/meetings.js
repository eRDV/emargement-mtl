import { RETRIEVE_MEETINGS } from "../actions/types";

const initialState = [];

function meetingReducer(meetings = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case RETRIEVE_MEETINGS:
      return payload;

    // case RETRIEVE_PARTICIPANTS:
    //   return payload;

    default:
      return meetings;
  }
}

export default meetingReducer;
