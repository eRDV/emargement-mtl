import { RETRIEVE_PARTICIPANTS } from "../actions/types";

const initialState = [];

function participantsReducer(participants = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case RETRIEVE_PARTICIPANTS:
      return payload;

    default:
      return participants;
  }
}

export default participantsReducer;
