import {
    CREATE_PERSON,
    RETRIEVE_PERSONS,
    UPDATE_PERSON,
    DELETE_PERSON,
    DELETE_ALL_PERSONS,
  } from "../actions/types";
  
  const initialState = [];
  
  function personReducer(persons = initialState, action) {
    const { type, payload } = action;
  
    switch (type) {
      case CREATE_PERSON:
        return [...persons, payload];
  
      case RETRIEVE_PERSONS:
        return payload;
  
      case UPDATE_PERSON:
        return persons.map((person) => {
          if (person.id === payload.id) {
            return {
              ...person,
              ...payload,
            };
          } else {
            return person;
          }
        });
  
      case DELETE_PERSON:
        return persons.filter(({ id }) => id !== payload.id);
  
      case DELETE_ALL_PERSONS:
        return [];
  
      default:
        return persons;
    }
  };
  
  export default personReducer;