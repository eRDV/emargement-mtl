import http from "../http-common";

class MeetingDataService {
  getAll(id) {
    return http.get(`/meetings/${id}`);
  }
  getParticipants(uuid,next_page_token){
    return http.get(`/meetings/participants/${uuid}?next_page_token=${next_page_token}`);
  }
  getAccountMettings(email){
    return http.get(`/meetings/account/${email}`);
  }
}

export default new MeetingDataService();